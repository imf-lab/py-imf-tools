# py-imf-tools

Python tools for working with the Information Modelling Framework (IMF).

 - imftype-shacl2owl.py translates a file with IMF Type SHACL Shapes into an OWL class representation.
 - imftype-shacl2rdf.py translates a file with IMF Type SHACL Shapes into prototypical RDF IMF instance data.