import sys

import rdflib
from rdflib import URIRef, BNode
from rdflib.namespace import NamespaceManager, OWL, SH, RDFS, RDF, DC, SKOS, VANN

ontologyFile = sys.argv[1]
shaclFile = sys.argv[2]
outputFile = sys.argv[3]

ontology = rdflib.Graph()
ontology.parse(ontologyFile, format="ttl")
nm = NamespaceManager(ontology)

shacl = rdflib.Graph()
shacl.parse(shaclFile, format="ttl")


def qname(x):
    if type(x) == URIRef:
        return nm.normalizeUri(x)
    elif type(x) == BNode:
        return "blank"
    else:
        return x


def texEnv(env, s):
    if s != '':
        return "\n\\begin{" + env + "}\n" + s.strip() + "\n\end{" + env + "}\n\n"
    else:
        return s


def texItem(term, predicate, name, joiner="\n\n", dir="X", graph=ontology, env='markdown'):
    if dir == "subjects":
        values = graph.subjects(predicate, term)
    else:
        values = graph.objects(term, predicate)

    values = list(filter(lambda x: type(x) != BNode, list(values)))

    if not bool(values):
        return ''
    else:
        if env == 'uris':
            values = map(lambda x: "\\texttt{" + x + "}", sorted(map(qname, values)))
            return "\item[" + name + "] " + joiner.join(values) + "\n"
        else:
            return "\item[" + name + "] " + texEnv(env, joiner.join(values))


output = ""

# Get ontology metadata

txtOntology = ""
iriOntology = URIRef("http://ns.imfid.org/imf#IMFOntology")

txtOntology += str(ontology.value(iriOntology, DC.description)) + "\n\n"
txtOntology += str(ontology.value(iriOntology, SKOS.scopeNote))
output += texEnv('markdown', txtOntology)

# Ontology summary

output += "The ontology defines the following terms, categorised by their OWL type:\n\n"
txtEntities  = texItem(OWL.Class, RDF.type, "Classes", joiner=", ", dir="subjects", env='uris')
txtEntities += texItem(OWL.ObjectProperty, RDF.type, "Object properties", joiner=", ", dir="subjects", env='uris')
txtEntities += texItem(OWL.DatatypeProperty, RDF.type, "Data properties", joiner=", ", dir="subjects", env='uris')
txtEntities += texItem(OWL.AnnotationProperty, RDF.type, "Annotation properties", joiner=", ", dir="subjects", env='uris')
txtEntities += texItem(OWL.NamedIndividual, RDF.type, "Individuals", joiner=", ", dir="subjects", env='uris')
output += texEnv('description', txtEntities)

# Term definitions

groups = sorted(ontology.objects(None, VANN.termGroup, True), reverse=True)

output += "The terms are presented below, categorised in informal groups according to topic: " + texEnv('compactitem', "\n".join(map(lambda x: "\item " + x.capitalize(), groups)))

output += "Each group contains an informal diagram of the group's terms. Each term is presented with a selection of metadata, its basic OWL ontology definitions and the SHACL shape constraints defined for the term. The listing does not contain all OWL axioms and SHACL shape definitions as it is difficult to represent in condenced form, please consult the OWL ontology and SHACL shape description for all details.\n\n"

for group in groups:
    output += "\subsection{" + group.capitalize() + "}\n\n"
    output += "\\begin{figure}\centering\n"
    output += "\includegraphics[width=1\\textwidth]{img/ontology/imf-ontology-" + group + ".png}\n\caption{Ontology group: " + group + "}\n\label{fig:ontology-group-" + group + "}\n\end{figure}\n\n"

    for term in sorted(ontology.subjects(VANN.termGroup, group)):
        # metadata
        output += "\subsubsection{" + str(ontology.value(term, SKOS.prefLabel, None)) + "}\n"
        txtItems = "\item[IRI] \\texttt{" + qname(term) + "}\n"
        txtItems += texItem(term, SKOS.definition, "Definition")
        txtItems += texItem(term, SKOS.scopeNote, "Usage note")
        txtItems += texItem(term, SKOS.example, "Example")
        txtItems += texItem(term, SKOS.note, "Note")
        txtItems += texItem(term, RDF.type, "Type", joiner=", ", env='uris')
        txtItems += texItem(term, RDFS.subClassOf, "Superclass", joiner=", ", env='uris')
        txtItems += texItem(term, RDFS.subClassOf, "Subclass", joiner=", ", dir="subjects", env='uris')
        txtItems += texItem(term, RDFS.subPropertyOf, "Superproperty", joiner=", ", env='uris')
        txtItems += texItem(term, RDFS.subPropertyOf, "Subproperties", joiner=", ", dir="subjects", env='uris')
        txtItems += texItem(term, RDFS.domain, "Domain", joiner=", ", env='uris')
        txtItems += texItem(term, RDFS.range, "Range", joiner=", ", env='uris')
        txtItems += texItem(term, RDF.type, "Instances", joiner=", ", dir="subjects", env='uris')
        # txtItems += texItem(term, SKOS.editorialNote, "Editorial note")


        # SHACL
        txtShacl = ""
        boolShacl = False
        termShape = URIRef(str(term) + "Shape")
        shprops = ['path', 'class', 'minCount', 'maxCount', 'severity', 'message']
        shpropsN = ['path', 'class', 'min', 'max', 'severity', 'message']

        tblEnd = " \\\\ \n"
        txtShacl += "{|" + 'l|' * len(shprops) + "}\n"
        txtShacl += "\\hline\n" + " & ".join(shpropsN) + tblEnd + "\\hline\n"
        for property in sorted(shacl.objects(termShape, SH.property)):
            boolShacl = True
            txtShacl += " & ".join([qname(shacl.value(property, SH[shp], None, default="")) for shp in shprops]) + tblEnd
        txtShacl += "\\hline\n"

        if boolShacl:
            txtItems += "\item[SHACL Definition]~\\\\[.5ex]" + texEnv('tabular', txtShacl)

        output += texEnv('compactdesc', txtItems)

f = open(outputFile, "w")
f.write(output)
f.close()
